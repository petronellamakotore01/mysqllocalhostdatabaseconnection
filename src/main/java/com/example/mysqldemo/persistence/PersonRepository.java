package com.example.mysqldemo.persistence;

import com.example.mysqldemo.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
